import MealItem from "./Components/MealItem/MealItem";
import mealImage from './assets/meal.png';
import drinkImage from './assets/cup.png';
import './App.css';
import {useState} from "react";
import MealOrder from "./Components/MealOrder/MealOrder";
import {nanoid} from "nanoid";

function App() {

    const meals = [
        {name: 'Hamburger', price: 80, image: mealImage, count: 0, id: nanoid()},
        {name: 'Cheeseburger', price: 90, image: mealImage, count: 0,id: nanoid()},
        {name: 'Fries', price: 40, image: mealImage,count: 0,id: nanoid()},
        {name: 'Coffee', price: 50, image: drinkImage,count: 0,id: nanoid()},
        {name: 'Tea', price: 30, image: drinkImage,count: 0,id: nanoid()},
        {name: 'Cola', price: 40, image: drinkImage,count: 0,id: nanoid()},
    ];

    const [Meals,setMeals] = useState(meals);

    const addOrder =(id)=>{
        setMeals(Meals.map(meal => {
            if(meal.id === id) {
                return {
                    ...meal,
                    count: meal.count+1,
                };
            }
            return meal;
        }));
    }

    const deleteOrder =(id)=>{
        setMeals(Meals.map(meal => {
            if(meal.id === id) {
                return {
                    ...meal,
                    count: meal.count-1,
                };
            }
            return meal;
        }));
    }

    const allTypesOfMeal = Meals.map((meal,index)=>(
        <MealItem
            key = {index}
            name={meal.name}
            price = {meal.price}
            image = {meal.image}
            onMakeOrder = {()=>addOrder(meal.id)}
        />
    ))


  return (
    <div className="App">
        <fieldset className='box'>
            <legend>Order Details</legend>
            <MealOrder
                orders={Meals}
                onDelete ={deleteOrder}
            />
        </fieldset>
        <fieldset className='box'>
            <legend>Add items</legend>
            <div className='allMeals'>
                {allTypesOfMeal}
            </div>
        </fieldset>

    </div>
  );
}

export default App;
