import React from 'react';
import './MealOrder.css';

const MealOrder = props => {
console.log(props.orders);
    const orders = props.orders.filter(meal=>meal.count>0);
    console.log(orders);

    const total = orders.reduce((acc,order)=>acc+order.price*order.count,0)

    const ordersToshow = orders.map(order =>(
        <div className='order-detail'>
            <span> {order.name}</span>
            <span> {order.count} x </span>
            <span> {order.price} KGS</span>
            <button className='delete-btn' onClick={()=>props.onDelete(order.id)}> Delete </button>
        </div>
    ))

    return (
        <div>
            {ordersToshow}
        <p><b>CurrentTotal Price: </b>{total}</p>
        </div>
    );
};

export default MealOrder;