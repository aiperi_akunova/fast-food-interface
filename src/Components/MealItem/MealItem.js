import React from 'react';
import './MealItem.css';

const MealItem = props => {
    return (
        <div className='oneMeal' onClick={props.onMakeOrder}>
            <div> <img alt='type of meal' src={props.image}/></div>
            <div>
                <h4>{props.name}</h4>
                <p><b>Price: </b>{props.price} KGS</p>
            </div>
        </div>
    );
};

export default MealItem;